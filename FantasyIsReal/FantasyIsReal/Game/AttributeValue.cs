﻿namespace Game
{
    public class AttributeValue
    {
        public Attribute Attribute { get; set; }
        public int Value { get; set; }
        public AttributeValue(Attribute atribute, int value)
        {
            Attribute = atribute;
            Value = value;
        }
    }
}