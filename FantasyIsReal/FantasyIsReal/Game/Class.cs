﻿namespace Game
{
    public enum Class
    {
        Warrior,
        Monk,
        Paladin,
        Druid,
        Rogue,
        Mage
    }
}