﻿using System;
namespace Game
{
    public class Person
    {
        public string Name { get; set; }
        public Class @class { get; set; }
        public int Level { get; set; }
        public int Experience { get; set; }
        public int ExpToLevel { get; set; }
        public int HPmax { get; set; }
        public int HP { get; set; }
        public int MPmax { get; set; }
        public int MP { get; set; }
        public int BaseAttack { get; set; }
        public int BaseDefend { get; set; }
        public AttributeValue Strength { get; set; }
        public AttributeValue Agility { get; set; }
        public AttributeValue Intelligence { get; set; }
        public AttributeValue Endurance { get; set; }
        public Person(string name, AttributeValue[] attributeValue)
        {
            this.Name = name; @class = Class.Warrior;
            Level = 1; Experience = 0; ExpToLevel = 100;
            Strength = new AttributeValue(Attribute.Streght, 1);
            Agility = new AttributeValue(Attribute.Agility, 1);
            Intelligence = new AttributeValue(Attribute.Intelligence, 1);
            Endurance = new AttributeValue(Attribute.Endurance, 1);
            foreach (AttributeValue item in attributeValue)
            {
                AddAttribute(item);
            }
            HPmax = Endurance.Value + Strength.Value / 2 + 35; HP = HPmax;
            MPmax = Intelligence.Value * 2 + 4; MP = MPmax;
            BaseAttack = (Strength.Value + Agility.Value) / 2 + 2;
            BaseDefend = 1 - (int)Math.Pow(0.94, Agility.Value);
            int AllAttribute = Strength.Value + Agility.Value + Intelligence.Value + Endurance.Value;
            double strength = Strength.Value / AllAttribute; double agility = Agility.Value / AllAttribute;
            double intelligence = Intelligence.Value / AllAttribute; double endurance = Endurance.Value / AllAttribute;
            if (strength > agility && strength > intelligence && strength > endurance)
            {
                if (endurance > agility && endurance > intelligence)
                {
                    @class = Class.Warrior;
                }
                if (agility > endurance && agility > intelligence)
                {
                    @class = Class.Rogue;
                }
                if (intelligence > endurance && intelligence > agility)
                {
                    @class = Class.Druid;
                }
            }
            if (agility > strength && agility > intelligence && agility > endurance)
            {
                if (strength > intelligence && strength > endurance)
                {
                    @class = Class.Rogue;
                }
                if (intelligence > strength && intelligence > endurance)
                {
                    @class = Class.Mage;
                }
                if (endurance > strength && endurance > intelligence)
                {
                    @class = Class.Monk;
                }
            }
            if (intelligence > strength && intelligence > agility && intelligence > endurance)
            {
                if (strength > agility && strength > endurance)
                {
                    @class = Class.Druid;
                }
                if (agility > strength && agility > endurance)
                {
                    @class = Class.Mage;
                }
                if (endurance > strength && endurance > agility)
                {
                    @class = Class.Paladin;
                }
            }
            if (endurance > strength && endurance > agility && endurance > intelligence)
            {
                if (strength > agility && strength > intelligence)
                {
                    @class = Class.Warrior;
                }
                if (agility > strength && agility > intelligence)
                {
                    @class = Class.Monk;
                }
                if (intelligence > strength && intelligence > agility)
                {
                    @class = Class.Paladin;
                }
            }
        }
        public void AddAttribute(AttributeValue attributeValue)
        {
            if (attributeValue.Attribute == Attribute.Streght)
            {
                Strength.Value += attributeValue.Value;
            }
            if (attributeValue.Attribute == Attribute.Agility)
            {
                Agility.Value += attributeValue.Value;
            }
            if (attributeValue.Attribute == Attribute.Intelligence)
            {
                Intelligence.Value += attributeValue.Value;
            }
            if (attributeValue.Attribute == Attribute.Intelligence)
            {
                Endurance.Value += attributeValue.Value;
            }
        }
        public int GiveDamage()
        {
            return Strength.Value + Agility.Value / 2 + 1;
        }
        public void GetDamage(int damage)
        {
            HP = HP - damage * (1 - BaseDefend);
        }
        public void GetExperience()
        {
            Experience = +100;
            if (Experience == ExpToLevel)
            {
                Level = +1;
                ExpToLevel = (ExpToLevel + 100) * 2;
            }
        }
    }
}