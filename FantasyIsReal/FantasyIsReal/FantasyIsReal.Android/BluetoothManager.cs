﻿using System;
using Android.Bluetooth;
using Java.IO;
using Java.Util;
namespace FantasyIsReal.Droid
{
    class BluetoothManager
    {
        private const string UuidUniversePrifile = "00001101-0000-1000-8000-00805F9B34FB";
        private BluetoothDevice _result;
        private BluetoothSocket _mSocket;
        private BufferedReader _reader;
        private System.IO.Stream _mStream;
        private InputStreamReader _mReader;
        public BluetoothManager()
        {
            _result = null;
        }
        public String GetDataFromDevice()
        {
            _mStream.Write(new byte[] { 0 }, 0, 10);
            return _reader.ReadLine();
        }
        private void Close(IDisposable aConnectedObject)
        {
            if (aConnectedObject == null)
            {
                return;
            }
            try
            {
                aConnectedObject.Dispose();
            }
            catch (Exception)
            {
                throw;
            }
            aConnectedObject = null;
        }
        private void OpenDeviceConnection(BluetoothDevice btDevice)
        {
            try
            {
                _mSocket = btDevice.CreateInsecureRfcommSocketToServiceRecord(UUID.FromString(UuidUniversePrifile));
                _mSocket.Connect();
                _mStream = _mSocket.InputStream;
                _mReader = new InputStreamReader(_mStream);
                _reader = new BufferedReader(_mReader);
            }
            catch (IOException e)
            {
                Close(_mSocket);
                Close(_mStream);
                Close(_mReader);
                throw e;
            }
        }
        public void GetAllPairedDevices()
        {
            BluetoothAdapter btAdapter = BluetoothAdapter.DefaultAdapter;
            var devices = btAdapter.BondedDevices;
            if (devices != null && devices.Count > 0)
            {
                foreach(var item in devices)
                {
                    OpenDeviceConnection(item);
                }
            }
        }
    }
}