﻿using System;
using Android.App;
using Android.Content.PM;
using Android.OS;
namespace FantasyIsReal.Droid
{
    [Activity(Label = "FantasyIsReal", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        private String _data = null;

        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App());

            //BluetoothManager manager = new BluetoothManager();
            //manager.GetAllPairedDevices();

            //Thread thread = new Thread(() =>
            //{
            //    while (true)
            //    {
            //        _data = manager.GetDataFromDevice();
            //    }
            //}
            //);
            //thread.IsBackground = true;
            //thread.Start();
        }
    }
}