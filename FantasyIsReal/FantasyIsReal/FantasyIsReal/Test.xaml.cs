﻿using Game;
using System;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
namespace FantasyIsReal
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Test : ContentPage
	{
        private const int CLASTER_LENGHT = 4;
        private const int CLASTER_NUMBERS = 2;
        private const int QUESTION_NUMBERS = 14;
        private string[] _questions = new string[QUESTION_NUMBERS] 
        {
            "Вы проснулись утром, ваши первые действия?",
            "По дороге домой вы встретили человека, навясчего пристающего к вам. Ваши действия?",
            "Вы заблудились в лесу, как вы будете действать, чтобы выбраться?",
            "Ваши действия, если на вашей работе начальник сказал поработать сверхурочно без оплаты:",
            "Вы любите читать книги?",
            "Вы ходите в спортивный/тренажёрный зал заниматься?",
            "Ходите ли вы в походы?",
            "Много ли у вас друзей?",
            "Участввали ли вы в олимпиадах по учебным предметам?",
            "Участвовали ли вы в спортивных сревнованиях?",
            "Любите ли вы путешествовать по разным странам?",
            "Владеете ли вы способностью коммуницировать с людьмми?",
            "Выберете наиболее предпочтительный спорт:",
            "Выберите любимый жанр книг / фильмов:"
        };
        private string[][] _answers = new string[QUESTION_NUMBERS][]
        {
            new string[] { "Зарядка#a1", "Пробежка#e1", "Завтрак#s1", "Работа за компьютером#i1", "Следую расписанию дня#i2" },
            new string[] { "Угроза#s1", "Просьба отстать от вас#e1", "Агрессивное поведение#s2", "Провление сдержанности и безразличия#a1", "Молчание и отстранение от человека#i1" },
            new string[] { "Буду полагаться на себя и пойду в выбранном напрвлении#s1", "Спрошу у лесника или буду следовать тропинкам#a1", "Без паники я знаю как выжить и если я не выбирусь меня успеют найти#e1", "Воспользуюсь навигатором#i1", "Я хорошо ориентируюсь на местности и точно знаю как выйти из леса#e2" },
            new string[] { "Соглашусь#e1", "Откажусь, под предлогом работы на другом проекте#a1", "Обговорю предложения со своими условиями работы#a2", "Полный отказ#s1", "Выразить желание уйти с проекта и перейти на другой более выгодный проект#i1" },
            new string[] { "Да#i1", "Нет#" },
            new string[] { "Да#s1", "Нет#" },
            new string[] { "Да#e1", "Нет#" },
            new string[] { "Да#a1", "Нет#" },
            new string[] { "Да#i1", "Нет#" },
            new string[] { "Да#s1", "Нет#" },
            new string[] { "Да#e1", "Нет#" },
            new string[] { "Да#a1", "Нет#" },
            new string[] { "Боевые искуства#s1", "Тяжёлая атлетика#s2", "Прыжки в высоту / в длину#a1", "Гимнастика#a2", "Бег на дистанцию#e1", "Плавание#e2", "Люблю только по телику фотболл смотреть#i1", "Не увлекюсь спортом#i2" },
            new string[] { "Боевик#s1", "Фантастика#i1", "Научные#i2", "Публитистические#e2", "Приключение#e1", "Психологический#a1", "Детектив#a2", "Не люблю читать и смотреть#s2" },
        };
        private int _index = 0;
        private Random _rnd = new Random();
        private TestAtribute testAtribute = new TestAtribute();
		public Test()
		{
			InitializeComponent();
            Question.Text = _questions[_index];
            string[] answsers = new string[_answers[_index].Length];
            for(int i = 0; i < _answers[_index].Length; i++)
            {
                answsers[i] = (i + 1) + "." + _answers[_index][i].Split('#')[0];
            }
            Answers.ItemsSource = answsers.ToArray();            
        }
        private void Answers_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item != null)
            {
                if (((string)e.Item).Split('.')[1] != "Нет")
                {
                    int numAnswer = Convert.ToInt32(((string)e.Item).Split('.')[0]) - 1;
                    Answer answer = new Answer(((string)e.Item).Split('.')[1]);
                    testAtribute.Questions.Add(new Question(_questions[_index]));
                    testAtribute.Questions[testAtribute.Questions.Count - 1].Answers.Add(answer);
                    string atrib = _answers[_index][numAnswer].Split('#')[1];
                    if (atrib[0] == 's')
                    {
                        answer.Bonuses.Add(new AttributeValue(Game.Attribute.Streght, atrib[1] - 48));
                    }
                    else if (atrib[0] == 'a')
                    {
                        answer.Bonuses.Add(new AttributeValue(Game.Attribute.Agility, atrib[1] - 48));
                    }
                    else if (atrib[0] == 'e')
                    {
                        answer.Bonuses.Add(new AttributeValue(Game.Attribute.Endurance, atrib[1] - 48));
                    }
                    else if (atrib[0] == 'i')
                    {
                        answer.Bonuses.Add(new AttributeValue(Game.Attribute.Intelligence, atrib[1] - 48));
                    }
                    NextQuestion();
                }
                else
                {
                    NextInClaster();
                }
            }
        }
        private void NextInClaster()
        {
            if(_index < 8)
            {
                _index = 4 + _rnd.Next(0, CLASTER_LENGHT);
            }
            else
            {
                _index = 8 + _rnd.Next(0, CLASTER_LENGHT);
            }
            Question.Text = _questions[_index];
            string[] answsers = new string[_answers[_index].Length];
            for (int i = 0; i < _answers[_index].Length; i++)
            {
                answsers[i] = (i + 1) + "." + _answers[_index][i].Split('#')[0];
            }
            Answers.ItemsSource = answsers.ToArray();
        }
        private void NextQuestion()
        {
            if(_index < 3)
            {
                _index++;
            }
            else if(_index == 3)
            {
                _index = _index + _rnd.Next(0, CLASTER_LENGHT);
            }
            else if(_index <= 7)
            {
                _index = 8 + _rnd.Next(0, CLASTER_LENGHT);
            }
            else if(_index < 12)
            {
                _index = 12;
            }
            else if(_index > 11 & _index < QUESTION_NUMBERS - 1)
            {
                _index++;
            }
            else if(_index >= QUESTION_NUMBERS - 1)
            {
                ToYourAttributes();
                return;
            }
            Question.Text = _questions[_index];
            string[] answsers = new string[_answers[_index].Length];
            for (int i = 0; i < _answers[_index].Length; i++)
            {
                answsers[i] = (i + 1) + "." + _answers[_index][i].Split('#')[0];
            }
            Answers.ItemsSource = answsers.ToArray();
        }
        private async void ToYourAttributes()
        {
            for(int i = 0; i < testAtribute.Questions.Count; i++)
            {
                testAtribute.InterptetAnswer(i, 0);
            }
            Person person = new Person("Default", testAtribute.Attributes);
            await Navigation.PushAsync(new YourAttributes(person));
        }
	}
}