﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
namespace FantasyIsReal
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FirstGamePage : ContentPage
	{
        public FirstGamePage()
        {
            InitializeComponent();
        }
        private async void ToTest(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Test());
        }
        private async void ToGame(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new GameMain());
        }
    }
}