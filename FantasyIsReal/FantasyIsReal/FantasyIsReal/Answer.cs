﻿using System.Collections.Generic;
using Game;
namespace FantasyIsReal
{
    internal class Answer
    {
        public string Text { get; set; }
        public List<AttributeValue> Bonuses { get; set; }
        public Answer(string text)
        {
            Text = text;
            Bonuses = new List<AttributeValue>();
        }
    }
}