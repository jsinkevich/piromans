﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FantasyIsReal
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class GameMain : ContentPage
	{
		public GameMain ()
		{
			InitializeComponent ();
            List<string> players = new List<string>();
            players.Add("");
            Players.ItemsSource = players.ToArray();
        }
        public void Players_ItemTapped(object sender, EventArgs e)
        {
            if(((ItemTappedEventArgs) e).Item != null)
            {
            }
        }
    }
}