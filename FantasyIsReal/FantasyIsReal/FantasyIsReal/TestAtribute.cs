﻿using System.Collections.Generic;
using Game;
namespace FantasyIsReal
{
    class TestAtribute
    {
        public List<Question> Questions { get; set; }
        public AttributeValue[] Attributes { get; set; }
        int _questionNumber;
        public TestAtribute()
        {
            Questions = new List<Question>();
            _questionNumber = -1;
            Attributes = new AttributeValue[4];
            Attributes[0] = new AttributeValue(Game.Attribute.Streght, 1);
            Attributes[1] = new AttributeValue(Game.Attribute.Agility, 1);
            Attributes[2] = new AttributeValue(Game.Attribute.Intelligence, 1);
            Attributes[3] = new AttributeValue(Game.Attribute.Endurance, 1);
        }
        public Question NextQuestion()
        {
            _questionNumber++;
            if (_questionNumber < Questions.Count)
            {
                return Questions[_questionNumber];
            }
            return null;
        }   
        public void InterptetAnswer(int questionNum, int answerNum)
        {
            List<AttributeValue> pluses = Questions[questionNum].Answers[answerNum].Bonuses;
            foreach(var plus in pluses)
            {
                foreach(var attrib in Attributes)
                {
                    if (plus.Attribute == attrib.Attribute)
                    {
                        attrib.Value += plus.Value;
                    }
                }
            }
        }
    }
}