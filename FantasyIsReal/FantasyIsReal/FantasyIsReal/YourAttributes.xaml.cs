﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Game;
namespace FantasyIsReal
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class YourAttributes : ContentPage
	{
		public YourAttributes (Person person)
		{
			InitializeComponent ();
            string[] attributes = new string[11];
            attributes[0] = "Имя вашего персонажа - " + person.Name;
            attributes[1] = "Класс - " + person.@class;
            attributes[2] = "Уровень - " + person.Level;
            attributes[3] = "Опыт - " + person.Experience + "/" + person.ExpToLevel;
            attributes[4] = "Сила - " + person.Strength.Value;
            attributes[5] = "Ловкость - " + person.Agility.Value;
            attributes[6] = "Интелект - " + person.Intelligence.Value;
            attributes[7] = "Выносливость - " + person.Endurance.Value;      
            attributes[8] = "Здоровье - " + person.HP + " / " + person.HPmax;
            attributes[9] = "Мана - " + person.MP + " / " + person.MPmax;
            attributes[10] = "Атака - " + person.MP + " / " + person.MPmax;
            attributes[11] = "Защита - " + person.MP + " / " + person.MPmax;
            Attributes.ItemsSource = attributes;
		}
        private async void ToFGP()
        {
            await Navigation.PushAsync(new FirstGamePage());
        }
    }
}