﻿using System.Collections.Generic;
namespace FantasyIsReal
{
    class Question
    {
        public string Text { get; set; }
        public List<Answer> Answers { get; set; }
        public Question(string text)
        {
            Text = text;
            Answers = new List<Answer>();
        }
    }
}